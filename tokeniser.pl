#!/usr/bin/perl

# stupid tokeniser

use strict;
binmode(STDIN, ':utf8');
binmode(STDOUT, ':utf8');

while (my $line=<STDIN>) {
    chomp $line;
    $line =~ s{[\.,;?!]}{ $&}g;
    $line =~ s{'s\b}{ 's}g;
    print $line, "\n";
}
