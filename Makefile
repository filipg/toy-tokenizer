
TEST_INS=$(wildcard tests/*.in)
TEST_OUTS=$(patsubst tests/%.in, tests/%.out, $(TEST_INS))

all: check

check: run
	git diff --exit-code --word-diff -- tests/*.out

run: $(TEST_OUTS)

tests/%.out: tests/%.in tokeniser.pl
	./tokeniser.pl < $< > $@
